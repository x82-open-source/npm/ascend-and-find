import ascendAndFile from '../../src';
import path from 'path';
describe('Ascension', () => {
    it('Should find the file', async () => {
        let vals = await ascendAndFile(__dirname, 'package.json');

        let expectedDir = path.join(__dirname, '../../');
        expect(vals.dir).toEqual(expectedDir);
        expect(vals.path).toEqual(path.join(expectedDir, 'package.json'));
        expect(vals.stats).toBeTruthy();
    });
});
