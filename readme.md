# ascend-and-find

A module that recursively searches upwards to find a file or directory

## Installing

```bash
npm i --save ascend-and-find
```

## Usage

```
import ascendAndFind from 'ascend-and-find';


///...

try {
//Current working directory, and filename
let {dir, path, stats} = await ascendAndFind(__dirname,'fileTo.find');
//Returns the directory found in, the exact path string and the stat of the file

} catch (err) {
    //File was not found
}
```

### Licence
MIT 