import { promisify } from 'util';
import fs from 'fs';
import path from 'path';

export const callback = (cwd, name, callback) => {
	function startSearch() {
		fs.stat(path.join(cwd, name), function (err, stats) {
			if (err) {
				if (path.join(cwd, '..') === cwd) {
					return callback(new Error('Could not find : ' + name));
				} else {
					//Ascend
					cwd = path.join(cwd, '..');
					process.nextTick(startSearch);
				}
			} else {
				callback(null,
					{
						dir: `${cwd}${path.sep}`,
						path: path.join(cwd, name),
						stats: stats
					});
			}
		});
	}
	startSearch();
};

export default promisify(callback);